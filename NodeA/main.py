from network import LoRa, WLAN , Bluetooth
from machine import UART
import socket
import time
import struct
import ujson
#CONFIGURACION EL DISPOSITIVO
#tx_power 5-20 en 915MHz
import network
import time

# setup as a station

wlan1 = WLAN()
#lte = LTE()
blt1 = Bluetooth()
server = network.Server()


uart1 = UART(1,115200)
uart1.init(115200, bits=8, parity=None, stop=1)

#Desactivando los modulos
wlan1.deinit()
#lte.deinit()
blt1.deinit()
server.deinit()
#IP PRIVADA
#Clase A o class
#mascara 24
#Puerto = 1024

lora = LoRa(mode=LoRa.LORA,
            frequency=915000000,
            tx_power=16,
            bandwidth=LoRa.BW_500KHZ,
            sf=8,
            preamble=12,
            coding_rate=LoRa.CODING_4_7)
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

s.setblocking(False)
print("Sarting TX2")

contadora_re=0
while True:
    time.sleep(2)
    data=uart1.read(150)
    #events = lora.events()
    try:
        if data is not None:
            s.send(data)
            print(data[0:10])
    except Exception as e:
        print(e)
        continue
